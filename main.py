from lark import Lark

grammar = r"""
program: NEWLINE* (statement NEWLINE*)+

statement: 
| ifstatement elifstatement* elsestatement* 
| whilestatement 
| forstatement  
| arithmetic 
| assignment 
| array
| multilinecomment 
| inlinecomment

multilinecomment: THREEQUOTES (ANY_STRING |  NEWLINE )* THREEQUOTES

inlinecomment: HASH ANY_STRING* NEWLINE*

whilestatement: WHILE LPAREN? expressionnew RPAREN? COLON NEWLINE* (INDENT statement NEWLINE*)+

forstatement: FOR ID IN (ID | range) COLON NEWLINE* (INDENT statement NEWLINE*)+

range: RANFUN LPAREN FLOAT COMMA FLOAT RPAREN

ifstatement: IF expressionnew COLON NEWLINE* (INDENT statement NEWLINE*)+

elifstatement: ELIF expressionnew COLON NEWLINE* (INDENT statement NEWLINE*)+

elsestatement: ELSE COLON NEWLINE* (INDENT statement NEWLINE*)+

expressionnew: simpleexpression 
| expressionnew AND simpleexpression 
| expressionnew OR simpleexpression 
| NOT simpleexpression

simpleexpression: NOT? factor  
| NOT? factor OP factor
| LPAREN NOT? factor RPAREN 
| LPAREN NOT? factor OP factor RPAREN | TRUE

arithmetic: ID (arith_operator EQ expression)+

assignment: ID EQ (expression | STRING)

arith_operator: PLUS | MINUS | MULT | DIV | MOD

array: ID EQ LBRACK (array_item (COMMA array_item)*)? RBRACK

array_item: INT | FLOAT | STRING

expression: term ((PLUS | MINUS) term)*

term: factor ((MULT | DIV | MOD) factor)*

factor: INT | FLOAT | ID | LPAREN expression RPAREN 


AND : /and/
OR : /or/ 
NOT : /not/
EQ: /=/
PLUS: /\+/
MINUS: /-/
MULT: /\*/
DIV: /\//
MOD: /%/
LBRACK: /\[/
RBRACK: /\]/
COMMA: /,/
LPAREN: /\(/
RPAREN: /\)/
IF: /if/
ELIF: /elif/
ELSE: /else/
COLON: /:/
OP: /< | > | <= | >= | == | !=/
WHILE: /while/
HASH: /#/
THREEQUOTES: /\'\'\'/
FOR: /for/
IN: /in/
RANFUN: /range/
ANY_STRING: /[a-zA-Z0-9_.,']+/
NEWLINE: /\n/
INDENT: /    /
STRING: /'[^']*'/ | /"[^"]*"/ | /'[a-zA-Z0-9_]*'/ | /"[a-zA-Z0-9_]*"/
ID: /[a-zA-Z0-9_.]+/
FLOAT: /-?\d+(\.\d+)?([eE][-+]?\d+)?/
INT: /'-'? [0-9]+/
TRUE: /True/

%import common.WS_INLINE 
%ignore WS_INLINE
"""

parser = Lark(grammar,start='program' )

test_case="""
assign1 = "20"
assign2 = 123
assign3 = 1.23
assign4 = assign1

arith_op1 = 1 + 2
arith_op2 = 13 - 3
arith_op3 = 10 / arith_op1
arith_op4 = 4.2 * 10
arith_op5 = arith_op1 % arith_op2

arith_op1 += arith_op2
arith_op2 -= arith_op3
arith_op3 *= arith_op4
arith_op4 /= arith_op5

array1 = [1, 2, 3, 4, 5]
array2 = ['a', 'b', 'c']
array3 = [1.6, 2.7, 3.8, 4.9, 5.0]

var1 = 10
var2 = var1/2 + 5
var3 = var2 % 2
var4 = 1

flag = True

assign1 = ""

# Deliverable 2

if var1 > var2:
    arith_op1 = 1 + 2
    assign1 = "text data"

if var1 <= var2 and var3 == var4:
    arith_op1 = 1 + 2
    assign1 = "text data"
else:
    arith_op4 = 4.2 * 10
    arith_op3 *= arith_op4

data = 0

if var1 != var2 or var3 >= var4:
    flag = True
elif (not flag) and var3 > 10:
    flag = False
else:
    data = -1

# Deliverable 3  

\'''
    comment test 2
    adding more comments
    and more...
\'''

while data > 0 or data != 0:
    data = data - 1
    if True:
        a = "This is the weirdest code I have ever written"

for data in array1:
    if data < 0:
        while(data != 0):
            b = "This code doesn't make any sense"
            data = data + 1
    elif data > 0:
        while(data != 0):
            c = "I feel like I have no purpose..."
            data = data - 1
for i in range(0,5):
    data = data * 2
    data = data - 1

\''' I need help, this code shouldn't even exist \'''
while True:
    data = 30
    data = data - 1
"""

def test():
    tree = parser.parse(test_case).pretty()
    print(tree)

    with open('tree.txt', 'w') as file:
        file.write(tree)
    

if __name__ == '__main__':
    test()