Description:
This project is a parser for context-free grammars. Inside the project, we define the context-free grammar. Then, we pass it to a parser object, which creates our parser by running the grammar. After that, we define our test case, which is a string passed to the parser object for testing. On failure, indicating any errors within the test case or the grammar, it produces an error. Otherwise, it returns a tree object outlining how the test case corresponds to the grammar. We then print the tree and also write it to a file. This project runs on Python and uses an external library that provides the parser object for us, called Lark.

Setup:

First, we need to install everything we need, which is the Lark library.
1a. For Mac, we just run pip install lark.
That's about it for the setup, as long as you have Python installed.
Define your test case. If you plan to adjust the grammar, you can go up to line 3 where it's defined and adjust it. To change the test case, go to line 99 and modify it inside the string.
If you want the output to be saved to a file, comment out line 185. If you want it to be printed out, comment out lines 187 and 188. If you want both options, don't make any further changes.
Run:

To run it, open up a terminal, navigate to where this project is located, and run the command python3 main.py. This will give you the tree output based on your selected output.
